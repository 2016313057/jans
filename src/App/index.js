import React from "react";
import { TareaProvider } from "../TareaContext";
import { AppUI } from "./AppUI";
import Login from "../Login";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function App() {
  return (
    <Router>
      <TareaProvider>
        <Routes>
          <Route path="/tasks" element={<AppUI />} />
          <Route exact path="/" element={<Login />} />
        </Routes>
      </TareaProvider>
    </Router>
  );
}

export default App;
